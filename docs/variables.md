## Role Variables

* `vector_version`: `0.12.2` - The vector version string see https://vector.dev/docs/setup/installation/package-managers/dpkg/



* `vector_member_docker_group`: `false` - if true, will add the vector user to the docker group



* `vector_member_systemd_journal_group`: `true` - if true, will add the vector user to the systemd-journal group



* `vector_member_additional_groups`: `[]` - a list of additional groups the vector user will be appended to



* `vector_sources`: `see defaults/main.yml` - yaml formatted vector source components, see https://vector.dev/docs/setup/configuration/



* `vector_sinks`: `see defaults/main.yml` - yaml formatted vector sink components, see https://vector.dev/docs/setup/configuration/



* `vector_transforms`: `{}` - yaml formatted vector transform components, see https://vector.dev/docs/setup/configuration/


